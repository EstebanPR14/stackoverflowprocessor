/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs.fsu.trans;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Javier
 */
public class Main {

    private static final String COMMAND = "E:\\Apps\\youtube-dl.exe --all-subs --skip-download --socket-timeout 60 -o F:\\subtitles\\%(id)s.%(ext)s https://www.youtube.com/watch?v=";

    public static void main2(String[] args) throws Exception {
        Process p = Runtime.getRuntime().exec(COMMAND + "5VyDsO0mFDU");
        int exit = p.waitFor();
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

        String line = "";
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }

    }

    public static void main(String[] args) throws IOException {
        File[] files = new File("C:\\Users\\Javier\\Documents\\NetBeansProjects\\android").listFiles((File dir, String name) -> name.endsWith(".csv"));

        ExecutorService ex = Executors.newFixedThreadPool(100);
        CompletionService<Boolean> cs = new ExecutorCompletionService<>(ex);

        class Task implements Callable<Boolean> {

            private final int num;
            private final String id;

            public Task(int num, String id) {
                this.num = num;
                this.id = id;
            }

            @Override
            public Boolean call() throws Exception {

                File[] possible = new File("F:\\subtitles").listFiles((File dir, String name) -> name.startsWith(id));
                if (possible.length > 0) {
                    System.out.println(num);
                    return true;
                }

                try {
                    Process p = Runtime.getRuntime().exec(COMMAND + id);
                    int exit = p.waitFor();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

                    String line = "";
                    while ((line = reader.readLine()) != null) {
//                        System.out.println(line);
                    }
                    System.out.println(num);
                    return exit == 0;
                } catch (IOException | InterruptedException e) {
                }
                System.out.println(num);
                return false;
            }

        }

        int submitted = 0;

        for (File file : files) {
            List<String> lines = Files.readAllLines(Paths.get(file.toURI()));
            for (String line : lines) {
                String[] tokens = line.split(";", -1);
                cs.submit(new Task(submitted, tokens[0]));
                submitted++;
            }
        }
        System.out.println("Submitted:" + submitted);

        int correct = 0;
        for (int i = 0; i < submitted; i++) {
            try {
                if (cs.take().get()) {
                    correct++;
                }
            } catch (InterruptedException | ExecutionException e) {

            }
        }

        System.out.println("Correct" + correct);
        ex.shutdown();

    }
}
