/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package TfIdf;

/**
 *
 * @author USER
 */
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Class to read documents
 *
 * @author Javier Escobar - Esteban Parra
 *
 * Adapted from:
 * http://computergodzilla.blogspot.com/2013/07/how-to-calculate-tf-idf-of-document.html
 */
public class DocumentParser {

    //This variable will hold all terms of each document in an array.
    private final ConcurrentHashMap<String, String[]> termsDocsArray = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, Integer> allTerms = new ConcurrentHashMap<>();
    private final List<Set<String>> docs = new ArrayList<>();
    private final ConcurrentHashMap<String, double[]> tfidfDocsVector = new ConcurrentHashMap<>();

    /**
     * Method to read files and store in array.
     *
     * @param filePath : source file path
     */
    public void parseFiles(String filePath) throws Exception {
        System.out.println("Parsing files...");
        File[] allfiles = new File(filePath).listFiles();
        ExecutorService ex = Executors.newFixedThreadPool(100);
        CompletionService cs = new ExecutorCompletionService(ex);

        class Parse implements Callable<Boolean> {

            int num;
            File file;

            public Parse(int num, File file) {
                this.num = num;
                this.file = file;
            }

            @Override
            public Boolean call() throws Exception {
                try {
                    String text = new String(Files.readAllBytes(Paths.get(file.toURI())));
                    String[] tokenizedTerms = text.replaceAll("[\\W&&[^\\s]]", "").split("\\W+");   //to get individual terms
                    for (String term : tokenizedTerms) {
                        if (!allTerms.containsKey(term)) {  //avoid duplicate entry
                            allTerms.put(term, 0);
                        }
                        allTerms.put(term, allTerms.get(term) + 1);
                    }
                    termsDocsArray.put(file.getName(), tokenizedTerms);
                    docs.add(new TreeSet<String>(Arrays.asList(tokenizedTerms)));
                    if (num % 10000 == 0) {
                        System.out.println(num);
                    }
                    return true;
                } catch (Exception ex) {
                    return false;
                }

            }

        }
        int index = 0;
        for (File f : allfiles) {
            double coin = Math.random();
            if (coin < 0.5) {
                cs.submit(new Parse(index, f));
                index++;
            }
            if (index == 10000) {
                break;
            }

        }
        for (int i = 0; i < index; i++) {
            cs.take();
        }
        ex.shutdown();
        System.out.println("Size:" + allTerms.size());
        List<String> toRemove = new ArrayList<>();
        for (String key : allTerms.keySet()) {
            if (allTerms.get(key) == 1) {
                toRemove.add(key);
            }
        }
        for (String remove : toRemove) {
            allTerms.remove(remove);
        }
        int position = 0;
        for (String key : allTerms.keySet()) {
            allTerms.put(key, position);
            position++;
        }
        System.out.println("Size:" + allTerms.size());
        System.out.println("Done!!");

    }

    /**
     * Method to create termVector according to its tfidf score.
     */
    public void tfIdfCalculator() throws Exception {
        System.out.println("Doing tfidf");

        ExecutorService ex = Executors.newFixedThreadPool(32);
        CompletionService cs = new ExecutorCompletionService(ex);

        class Parse2 implements Callable<Boolean> {

            int num;
            String[] docTermsArray;
            String filename;

            public Parse2(int num, String[] docTermsArray, String filename) {
                this.num = num;
                this.docTermsArray = docTermsArray;
                this.filename = filename;
            }

            @Override
            public Boolean call() throws Exception {
                try {
                    double tf; //term frequency
                    double idf; //inverse document frequency
                    double[] tfidfvectors = new double[allTerms.size()];
                    String[] terms = termsDocsArray.get(filename);
                    Set<String> newTerms = new TreeSet<>(Arrays.asList(terms));
                    for (String term : newTerms) {
                        tf = new TfIdf().tfCalculator(terms, term);
                        idf = new TfIdf().idfCalculator(docs, term);
                        Integer position = allTerms.get(term);
                        if (position != null) {
                            tfidfvectors[position] = tf * idf;
                        }
                    }
                    tfidfDocsVector.put(filename, tfidfvectors);  //storing document vectors;                                           
                    if (num % 1000 == 0) {
                        System.out.println(num);
                    }
                    return true;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    return false;
                }

            }

        }
        int index = 0;
        for (String filename : termsDocsArray.keySet()) {
            cs.submit(new Parse2(index, termsDocsArray.get(filename), filename));
            index++;
        }
        for (int i = 0; i < index; i++) {
            cs.take();
        }
        ex.shutdown();
        System.out.println("Done!!");

    }

    /**
     * Method to calculate cosine similarity between all the documents.
     */
    /**
     * Get for tfIfdVector
     *
     * @return The TfIdf Map
     */
    public ConcurrentHashMap<String, double[]> getTfidfDocsVector() {
        return tfidfDocsVector;
    }
}
