package TfIdf;

import java.util.List;
import java.util.Set;

/**
 * Class to calculate TfIdf of term.
 *
 * @author Javier Escobar - Esteban Parra
 *
 * Adapted from:
 * http://computergodzilla.blogspot.com/2013/07/how-to-calculate-tf-idf-of-document.html
 */
public class TfIdf {

    /**
     * Calculates the tf of term termToCheck
     *
     * @param totalterms : Array of all the words under processing document
     * @param termToCheck : term of which tf is to be calculated.
     * @return tf(term frequency) of term termToCheck
     */
    public double tfCalculator(String[] totalterms, String termToCheck) {
        double count = 0;  //to count the overall occurrence of the term termToCheck
        for (String s : totalterms) {
            if (s.equalsIgnoreCase(termToCheck)) {
                count++;
            }
        }
        return count;
    }

    public double idfCalculator(List<Set<String>> docs, String termToCheck) {
        double count = 0;

        count = docs.stream().filter((doc) -> (doc.contains(termToCheck)))
                .map((_item) -> 1.0)
                .reduce(count, (accumulator, _item) -> accumulator + 1);

        return Math.log(docs.size() / (1 + count));
    }
}
