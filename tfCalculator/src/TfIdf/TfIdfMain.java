/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package TfIdf;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 *
 * @author Javier Escobar - Esteban Parra
 *
 * Adapted from:
 * http://computergodzilla.blogspot.com/2013/07/how-to-calculate-tf-idf-of-document.html
 */
public class TfIdfMain {

    /**
     * Main method
     *
     * @param args
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void main(String args[]) throws Exception {

        DocumentParser dp = new DocumentParser();
        dp.parseFiles("F:\\corpus"); // give the location of source file
        dp.tfIdfCalculator(); //calculates tfidf

        try (FileOutputStream fileOut = new FileOutputStream("F:\\tfidf.ser"); ObjectOutputStream out = new ObjectOutputStream(fileOut)) {
            out.writeObject(dp.getTfidfDocsVector());
            System.out.printf("Serialized data is saved in F:\\tfidf.ser");
        }
    }
}
