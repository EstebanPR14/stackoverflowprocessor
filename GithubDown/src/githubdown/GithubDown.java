/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package githubdown;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Javier
 */
public class GithubDown {

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {
        List<String> lines = Files.readAllLines(Paths.get(new File("F:\\github\\java_projects.txt").toURI()));
        ExecutorService ex = Executors.newFixedThreadPool(100);
        CompletionService<Boolean> cs = new ExecutorCompletionService<>(ex);

        class Task implements Callable<Boolean> {

            private final int num;
            private final String url;
            private final String name;

            public Task(int num, String url, String name) {
                this.num = num;
                this.url = url;
                this.name = name;
            }

            @Override
            public Boolean call() throws Exception {
                File file = new File("F:\\github\\" + name);
                if (file.exists()) {
                    return true;
                }
                try {
                    Process p = Runtime.getRuntime().exec("C:\\Program Files\\Git\\bin\\git.exe clone " + url + " F:\\github\\" + name);
                    int exit = p.waitFor();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        System.out.println(line);
                    }
                    System.out.println(num);
                    return exit == 0;
                } catch (IOException | InterruptedException e) {
                }
                System.out.println(num);
                return false;
            }

        }

        int submitted = 0;
        for (String line : lines) {
            String[] tokens = line.split(";", -1);
            cs.submit(new Task(submitted, tokens[1], tokens[0].replace("/", "--")));
            submitted++;
        }

        System.out.println("Submitted:" + submitted);

        int correct = 0;
        for (int i = 0; i < submitted; i++) {
            try {
                if (cs.take().get()) {
                    correct++;
                }
            } catch (InterruptedException | ExecutionException e) {

            }
        }

        System.out.println("Correct" + correct);
        ex.shutdown();
    }

}
