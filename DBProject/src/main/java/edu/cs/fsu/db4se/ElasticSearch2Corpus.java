package edu.cs.fsu.db4se;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileOutputStream;
import java.net.InetAddress;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

/**
 *
 * @author Javier
 */
class ElasticSearch2Corpus {

    public void doSO() throws Exception {
        File output = new File("F:\\corpus");
        if (!output.exists()) {
            output.mkdirs();
        }
        ExecutorService es = Executors.newFixedThreadPool(50);
        CompletionService cs = new ExecutorCompletionService(es);
        int submitted = 0;
        try (Client client = TransportClient.builder().build()
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300))) {

            QueryBuilder qb = QueryBuilders.typeQuery("post");
            SearchResponse response = null;
            ObjectMapper mapper = new ObjectMapper();
            int i = 0;
            final int SIZE = 1000;
            response = client.prepareSearch("sedocs")
                    .setQuery(qb).setScroll(new TimeValue(120 * 1000))
                    .setSize(SIZE)
                    .execute().actionGet();
            while (true) {
                for (SearchHit hit : response.getHits().getHits()) {
                    String sourceAsString = hit.getSourceAsString();
                    if (sourceAsString != null) {
                        Post post = mapper.readValue(sourceAsString, Post.class);
                        cs.submit(new Cleaner(post.getText(), hit.getId(), output));
                        submitted++;
                        if (submitted % 1000 == 0) {
                            System.out.println(submitted);
                        }
                    }
                }
                response = client.prepareSearchScroll(response.getScrollId()).setScroll(new TimeValue(60000)).execute().actionGet();
                //Break condition: No hits are returned
                if (response.getHits().getHits().length == 0) {
                    break;
                }
            }
        }

        System.out.println("Submitted " + submitted);
        for (int i = 0; i < submitted; i++) {
            try {
                cs.take().get();
            } catch (Exception e) {
            };
        }
        es.shutdown();
    }

    public void doIt() throws Exception {
        File output = new File("F:\\corpus");
        if (!output.exists()) {
            output.mkdirs();
        }
        ExecutorService es = Executors.newFixedThreadPool(50);
        CompletionService cs = new ExecutorCompletionService(es);
        int submitted = 0;
        try (Client client = TransportClient.builder().build()
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300))) {

            QueryBuilder qb = QueryBuilders.typeQuery("video");
            SearchResponse response = null;
            ObjectMapper mapper = new ObjectMapper();
            int i = 0;
            final int SIZE = 1000;
            response = client.prepareSearch("sedocs")
                    .setQuery(qb).setScroll(new TimeValue(120 * 1000))
                    .setSize(SIZE)
                    .execute().actionGet();
            while (true) {
                for (SearchHit hit : response.getHits().getHits()) {
                    String sourceAsString = hit.getSourceAsString();
                    if (sourceAsString != null) {
                        Video video = mapper.readValue(sourceAsString, Video.class);
                        cs.submit(new Cleaner(video.getTitle() + " " + video.getDescription() + " " + video.getTranscript(), hit.getId(), output));
                        submitted++;
                        if (submitted % 1000 == 0) {
                            System.out.println(submitted);
                        }
                    }
                }
                response = client.prepareSearchScroll(response.getScrollId()).setScroll(new TimeValue(60000)).execute().actionGet();
                //Break condition: No hits are returned
                if (response.getHits().getHits().length == 0) {
                    break;
                }
            }
        }

        System.out.println("Submitted " + submitted);
        for (int i = 0; i < submitted; i++) {
            try {
                cs.take().get();
            } catch (Exception e) {
            };
        }
        submitted = 0;
        try (Client client = TransportClient.builder().build()
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300))) {

            QueryBuilder qb = QueryBuilders.typeQuery("code");
            SearchResponse response = null;
            ObjectMapper mapper = new ObjectMapper();
            int i = 0;
            final int SIZE = 1000;
            response = client.prepareSearch("sedocs")
                    .setQuery(qb).setScroll(new TimeValue(120 * 1000))
                    .setSize(SIZE)
                    .execute().actionGet();
            while (true) {
                for (SearchHit hit : response.getHits().getHits()) {
                    String sourceAsString = hit.getSourceAsString();
                    if (sourceAsString != null) {
                        Code code = mapper.readValue(sourceAsString, Code.class);
                        cs.submit(new Cleaner(code.getCode(), hit.getId(), output));
                        submitted++;
                        if (submitted % 1000 == 0) {
                            System.out.println(submitted);
                        }
                    }
                }
                response = client.prepareSearchScroll(response.getScrollId()).setScroll(new TimeValue(60000)).execute().actionGet();
                //Break condition: No hits are returned
                if (response.getHits().getHits().length == 0) {
                    break;
                }
            }
        }

        System.out.println("Submitted " + submitted);
        for (int i = 0; i < submitted; i++) {
            try {
                cs.take().get();
            } catch (Exception e) {
            };
        }
        es.shutdown();

    }

    public static void main(String[] args) throws Exception {
        ElasticSearch2Corpus app = new ElasticSearch2Corpus();
        app.doSO();
        app.doIt();
    }
}

class Cleaner implements Callable<Boolean> {

    private final String text;
    private final String filename;
    private final File outputDir;

    public Cleaner(String text, String filename, File outputDir) {
        this.text = text;
        this.filename = filename;
        this.outputDir = outputDir;
    }

    @Override
    public Boolean call() throws Exception {
        LuceneCleaner cleaner = new LuceneCleaner();
        try (FileOutputStream fos = new FileOutputStream(new File(outputDir + File.separator + filename))) {
            fos.write(cleaner.processStringPorter(text).getBytes());
        }
        return true;
    }

}
