/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.cs.fsu.db4se;

/**
 *
 * @author Javier
 */
public class Main {

    public static void main(String[] args) {

        Main app = new Main();
        app.process(new String[]{"", "index", "github"});
    }

    private void process(String[] args) {
        String command = args[1];
        switch (command) {
            case "index":
                handle_index(args);
                break;
            case "corpus":
                handle_corpus(args);

        }
    }

    private void handle_index(String[] args) {
        String option = args[2];
        try {
            switch (option) {
                case "youtube":
                    new Youtube2ElasticSearch().doIt();
                    break;
                case "github":
//                    new Github2Java().doIt();
                    new Java2ElasticSearch().doIt();
            }
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

    private void handle_corpus(String[] args) {
        String option = args[2];
        try {
            switch (option) {
                case "youtube":
                    new ElasticSearch2Corpus().doIt();
            }
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }

}
