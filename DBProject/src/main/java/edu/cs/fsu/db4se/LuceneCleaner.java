package edu.cs.fsu.db4se;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.miscellaneous.WordDelimiterFilterFactory;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.analysis.util.TokenFilterFactory;

/**
 * Hello world!
 *
 */
public final class LuceneCleaner {

    public String processStringPorter(String input) throws Exception {
        Analyzer analyzer = new StandardAnalyzer();
        TokenStream tokenStream = analyzer.tokenStream(null, new StringReader(input));
        Map<String, String> args = new HashMap<>();
        args.put("generateWordParts", "1");
        args.put("generateNumberParts", "1");
        args.put("splitOnCaseChange", "1");
        args.put("splitOnNumerics", "1");
        args.put("stemEnglishPossessive", "1");
        TokenFilterFactory fact = new WordDelimiterFilterFactory(args);
        tokenStream = fact.create(tokenStream);
        tokenStream = new LowerCaseFilter(tokenStream);
        tokenStream = new StopFilter(tokenStream, EnglishAnalyzer.getDefaultStopSet());
        tokenStream = new StopFilter(tokenStream, new CharArraySet(Dictionaries.getEnglish(), true));
        tokenStream = new NumberFilter(tokenStream);
        tokenStream = new PorterStemFilter(tokenStream);

        StringBuilder sb = new StringBuilder();
        CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);
        tokenStream.reset();
        while (tokenStream.incrementToken()) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            String term = charTermAttribute.toString().replaceAll("\\W", "");
            if (term.trim().isEmpty()) {
                continue;
            }
            sb.append(term);
        }
        String output = sb.toString().trim();
        if (output.isEmpty()) {
            return null;
        } else {
            return output;
        }
    }

}
