package edu.cs.fsu.db4se;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

/**
 *
 * @author Javier
 */
public class SO2ElasticSearch {

    private static final String PREFIX = "http://stackoverflow.com/questions/";

    public void doit() throws FileNotFoundException, IOException, ClassNotFoundException {

        File cache = new File("cache" + File.separator + "so2es");
        if (!cache.exists()) {
            cache.mkdirs();
        }

        List<Post> posts;
        File soCache = new File(cache.getAbsolutePath() + File.separator + "soCache.obj");
        if (!soCache.exists()) {
            posts = new ArrayList<>();
            File[] soFiles = new File("C:\\Users\\Esteban\\Documents\\NetBeansProjects\\StackOverflowProcessor").listFiles((File dir, String name) -> name.endsWith(".txt"));
            for (File soFile : soFiles) {
                try {
                    String filename = soFile.getName().replace("question", "").replace(".txt", "");
                    String url = PREFIX + filename;
                    posts.add(new Post(filename, url, new String(Files.readAllBytes(Paths.get(soFile.toURI())))));
                } catch (Exception e) {

                }
            }
            try (FileOutputStream fos = new FileOutputStream(soCache);
                    ObjectOutputStream oos = new ObjectOutputStream(fos)) {
                oos.writeObject(posts);
            }
        } else {
            try (FileInputStream fis = new FileInputStream(soCache);
                    ObjectInputStream ois = new ObjectInputStream(fis)) {
                posts = (ArrayList<Post>) ois.readObject();
            }

        }

        try (Client client = TransportClient.builder().build()
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300))) {
            BulkRequestBuilder bulkRequest = client.prepareBulk();

            ObjectMapper mapper = new ObjectMapper();

            int size = 100;
            for (Post post : posts) {
                bulkRequest.add(client.prepareIndex("sedocs", "post")
                        .setSource(mapper.writeValueAsBytes(post))
                );
                size--;
                if (size == 0) {
                    size = 100;
                    BulkResponse bulkResponse = bulkRequest.execute().actionGet();
                    if (bulkResponse.hasFailures()) {
                        System.out.println(bulkResponse.buildFailureMessage());
                    }
                    bulkRequest = client.prepareBulk();
                }
            }

            BulkResponse bulkResponse = bulkRequest.execute().actionGet();
            if (bulkResponse.hasFailures()) {
                System.out.println(bulkResponse.buildFailureMessage());
            }
        }
    }

    public static void main(String[] args) throws Exception {
        SO2ElasticSearch app = new SO2ElasticSearch();
        app.doit();
    }

}
