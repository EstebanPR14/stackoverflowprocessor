package edu.cs.fsu.db4se;

import java.io.Serializable;

/**
 *
 * @author Javier
 */
public class Code implements Serializable {

    private String url;
    private String code;

    public Code() {
    }

    public Code(String url, String code) {
        this.url = url;
        this.code = code;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
