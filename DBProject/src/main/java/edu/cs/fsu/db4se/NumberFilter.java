package edu.cs.fsu.db4se;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.FilteringTokenFilter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author JavierRicardo
 */
class NumberFilter extends FilteringTokenFilter {

    private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
    Pattern digitPattern = Pattern.compile("\\d+");

    public NumberFilter(TokenStream in) {
        super(in);
    }

    @Override
    protected boolean accept() throws IOException {
        Matcher matcher = digitPattern.matcher(termAtt);
        return !matcher.matches();
    }

}
