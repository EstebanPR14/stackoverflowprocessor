package edu.cs.fsu.db4se;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import static java.nio.file.FileVisitResult.CONTINUE;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Javier
 */
class Github2Java {

    public void doIt() {
        File[] projects = new File("F:\\github").listFiles(File::isDirectory);
        File output = new File("E:\\jgithub");

        ExecutorService es = Executors.newFixedThreadPool(100);
        CompletionService<Boolean> cs = new ExecutorCompletionService<>(es);
        int submitted = 0;
        for (File project : projects) {
            cs.submit(new Visitor(submitted, project, output));
            submitted++;
        }
        int correct = 0;
        System.out.println("Submitted:" + submitted);

        for (int i = 0; i < submitted; i++) {
            try {
                if (cs.take().get()) {
                    correct++;
                }
            } catch (InterruptedException | ExecutionException e) {

            }
        }
        System.out.println("Correct:" + correct);
        es.shutdown();
    }
}

class Visitor implements Callable<Boolean> {

    private final int num;
    private final File dir;

    private final File output;

    public Visitor(int num, File dir, File output) {
        this.num = num;
        this.dir = dir;
        this.output = output;
    }

    @Override
    public Boolean call() throws Exception {
        final File newDir = new File(output + File.separator + dir.getName());
        if (!newDir.exists()) {
            newDir.mkdirs();
        }
        final Path dirPath = Paths.get(dir.toURI());
        try {
            Files.walkFileTree(Paths.get(dir.toURI()), new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file,
                        BasicFileAttributes attr) throws IOException {
                    if (attr.isRegularFile() & file.toString().endsWith(".java")) {
                        Path relative = dirPath.relativize(file);
                        Path parent = relative.getParent();
                        File outputDir = new File(newDir.getAbsolutePath() + File.separator + parent.toString());
                        if (!outputDir.exists()) {
                            outputDir.mkdirs();
                        }
                        FileUtils.copyFileToDirectory(file.toFile(), outputDir);
                    }
                    return CONTINUE;
                }
            });
            System.out.println(num);
            return true;
        } catch (Exception ex) {
            System.out.println(num);
            return false;
        }
    }
}
