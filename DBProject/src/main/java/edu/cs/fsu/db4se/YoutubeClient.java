package edu.cs.fsu.db4se;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.util.DateTime;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTube.Search;
import com.google.api.services.youtube.model.ResourceId;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.Video;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

/**
 * Print a list of videos matching a search term.
 *
 * @author Jeremy Walker
 * @author Javier Escobar
 */
public class YoutubeClient {

    /**
     * Define a global variable that identifies the name of a file that contains
     * the developer's API key.
     */
    private static final String PROPERTIES_FILENAME = "youtube.properties";

    private static final long NUMBER_OF_VIDEOS_RETURNED = 50;

    /**
     * Define a global instance of a Youtube object, which will be used to make
     * YouTube Data API requests.
     */
    private static YouTube youtube;

    /**
     * Search for videos on YouTube.
     *
     * @throws FileNotFoundException
     */
    public static void doIt() throws FileNotFoundException, IOException {

        //Print Console output to a file
        // Read the developer key from the properties file.
        Properties properties = new Properties();
        try {
            InputStream in = Search.class.getResourceAsStream("/" + PROPERTIES_FILENAME);
            properties.load(in);

        } catch (IOException e) {
            throw e;
        }

        // This object is used to make YouTube Data API requests. The last
        // argument is required, but since we don't need anything
        // initialized when the HttpRequest is initialized, we override
        // the interface and provide a no-op function.
        youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, Auth.JSON_FACTORY, (HttpRequest request) -> {
        }).setApplicationName("youtube-cmdline-search-sample").build();

        // Define the API request for retrieving search results.
        YouTube.Search.List search = youtube.search().list("id,snippet");
        YouTube.Videos.List videos = youtube.videos().list("snippet,contentDetails");
        videos.setKey(properties.getProperty("youtube.apikey"));

        // Set your developer key from the {{ Google Cloud Console }} for
        // non-authenticated requests. See:
        // {{ https://cloud.google.com/console }}
        String apiKey = properties.getProperty("youtube.apikey");
        search.setKey(apiKey);

        // Restrict the search results to only include videos. See:
        // https://developers.google.com/youtube/v3/docs/search/list#type
        search.setType("video");

        // To increase efficiency, only retrieve the fields that the
        // application uses.
        //mir
        String fields = "items(id/kind,"
                + "id/videoId,"
                + "snippet/title,"
                + "snippet/description,"
                + "snippet/publishedAt,"
                + "snippet/channelId"
                + "),"
                + "nextPageToken,"
                + "pageInfo";
        search.setFields(fields);
        search.setMaxResults(NUMBER_OF_VIDEOS_RETURNED);

        //set query
        search.setQ("java");

        //set caption options
        search.setVideoCaption("closedCaption");

        DateFormat format = new SimpleDateFormat("yyyy-MM");
        //Starting from 01/01/2005
//        Date startDate = new GregorianCalendar(2005, 0, 1).getTime();
        Date startDate = new GregorianCalendar(2014, 5, 1).getTime();
        Date finalDate = new GregorianCalendar(2016, 3, 30).getTime();
        //
//        Date startDate = new GregorianCalendar(2005, 6, 1).getTime();
//        Date finalDate = new GregorianCalendar(2005, 6, 29).getTime();

        Calendar calendar;
        SearchListResponse searchResponse;
        while (startDate.before(finalDate)) {
            calendar = new GregorianCalendar();
            calendar.setTime(startDate);
            //Last day of previous month
            calendar.add(Calendar.MONTH, -1); //Previous month
            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            search.setPublishedAfter(new DateTime(calendar.getTime()));

            calendar.setTime(startDate);
            //First day next of next month
            calendar.add(Calendar.MONTH, 1);
            startDate = calendar.getTime();
            search.setPublishedBefore(new DateTime(startDate));

            search.setMaxResults(NUMBER_OF_VIDEOS_RETURNED);
            searchResponse = search.execute();
            calendar.add(Calendar.MONTH, -1);
            System.out.println("Results for " + format.format(calendar.getTime()) + " : " + searchResponse.getPageInfo().getTotalResults());
            String tok;
            try (Writer output = new OutputStreamWriter(new FileOutputStream(new File("yt-info" + format.format(calendar.getTime()) + ".csv")), Charset.forName("UTF-8"))) {
                do {
                    //emir
                    List<SearchResult> searchResultList = searchResponse.getItems();
                    if (searchResultList != null) {
                        Iterator<SearchResult> iteratorSearchResults = searchResultList.iterator();
                        while (iteratorSearchResults.hasNext()) {
                            SearchResult singleVideo = iteratorSearchResults.next();
                            //More details about the video                                                       
                            ResourceId rId = singleVideo.getId();
                            videos.setId(rId.getVideoId());
                            Video video = (Video) videos.execute().getItems().get(0);
                            // Confirm that the result represents a video. Otherwise, the
                            // item will not contain a video ID.
                            if (rId.getKind().equals("youtube#video")) {
                                StringBuilder sb = new StringBuilder();
                                sb.append(rId.getVideoId())
                                        .append(";")
                                        .append("https://www.youtube.com/watch?v=")
                                        .append(rId.getVideoId())
                                        .append(";")
                                        .append(singleVideo.getSnippet().getTitle())
                                        .append(";")
                                        .append(singleVideo.getSnippet().getDescription())
                                        .append(";")
                                        .append(singleVideo.getSnippet().getPublishedAt().toStringRfc3339())
                                        .append(";")
                                        .append(singleVideo.getSnippet().getChannelId())
                                        .append(";")
                                        .append(video.getSnippet().getTags())
                                        .append(";")
                                        .append(video.getSnippet().getCategoryId())
                                        .append(";")
                                        .append(video.getContentDetails().getDuration())
                                        .append(";")
                                        .append(video.getContentDetails().getCaption())
                                        .append("\n");
                                output.write(sb.toString());
                            }
                        }
                    }
                    tok = searchResponse.getNextPageToken();
                    search.setPageToken(tok);
                    search.setMaxResults(NUMBER_OF_VIDEOS_RETURNED);
                    searchResponse = search.execute();
                } while (tok != null);
            }

        }
    }
}
