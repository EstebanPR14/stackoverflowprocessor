package edu.cs.fsu.db4se;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

/**
 *
 * @author Javier Escobar
 */
class Youtube2ElasticSearch {

    public void doIt() throws Exception {
        Map<String, String> transcripts = new TreeMap<>();
        Map<String, String[]> videos = new TreeMap<>();

        File cache = new File("cache" + File.separator + "yt2es");
        if (!cache.exists()) {
            cache.mkdirs();
        }

        File infoCache = new File(cache.getAbsolutePath() + File.separator + "infoCache.obj");
        if (!infoCache.exists()) {
            File[] infoFiles = new File("F:\\yt-info").listFiles((File dir, String name) -> name.endsWith(".csv"));
            for (File infoFile : infoFiles) {
                List<String> lines = Files.readAllLines(Paths.get(infoFile.toURI()));
                for (String line : lines) {
                    String[] tokens = line.split(";", -1);
                    videos.put(tokens[0].trim(), tokens);
                }
            }
            try (FileOutputStream fos = new FileOutputStream(infoCache);
                    ObjectOutputStream oos = new ObjectOutputStream(fos)) {
                oos.writeObject(videos);
            }
        } else {
            try (FileInputStream fis = new FileInputStream(infoCache);
                    ObjectInputStream ois = new ObjectInputStream(fis)) {
                videos = (Map<String, String[]>) ois.readObject();
            }

        }

        File transCache = new File(cache.getAbsolutePath() + File.separator + "transCache.obj");
        if (!transCache.exists()) {
            File[] transFiles = new File("F:\\transcripts").listFiles((File dir, String name) -> name.endsWith(".en.vtt"));
            for (File transFile : transFiles) {
                String text = new String(Files.readAllBytes(Paths.get(transFile.toURI())));
                transcripts.put(transFile.getName().replace(".en.vtt", ""), text);
            }
            try (FileOutputStream fos = new FileOutputStream(transCache);
                    ObjectOutputStream oos = new ObjectOutputStream(fos)) {
                oos.writeObject(transcripts);
            }
        } else {
            try (FileInputStream fis = new FileInputStream(transCache);
                    ObjectInputStream ois = new ObjectInputStream(fis)) {
                transcripts = (Map<String, String>) ois.readObject();
            }
        }

        try (Client client = TransportClient.builder().build()
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300))) {
            BulkRequestBuilder bulkRequest = client.prepareBulk();

            ObjectMapper mapper = new ObjectMapper();

            for (String transkKey : transcripts.keySet()) {
                String[] info = videos.get(transkKey);
                if (info != null) {
                    Video newVideo = new Video();
                    newVideo.setId(transkKey);
                    newVideo.setTitle(info[2]);
                    newVideo.setDescription(info[3]);
                    newVideo.setUrl(info[1]);
                    newVideo.setTranscript(transcripts.get(transkKey));

                    bulkRequest.add(client.prepareIndex("sedocs", "video")
                            .setSource(mapper.writeValueAsBytes(newVideo))
                    );
                }
            }

            BulkResponse bulkResponse = bulkRequest.execute().actionGet();
            if (bulkResponse.hasFailures()) {
                System.out.println(bulkResponse.buildFailureMessage());
            }
        }
    }
    
    public static void main(String[] args) throws Exception {
        Youtube2ElasticSearch app = new Youtube2ElasticSearch();
        app.doIt();
    }
}
