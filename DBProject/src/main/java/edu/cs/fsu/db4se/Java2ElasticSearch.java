package edu.cs.fsu.db4se;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.FileVisitResult;
import static java.nio.file.FileVisitResult.CONTINUE;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

/**
 *
 * @author Javier
 */
public class Java2ElasticSearch {

    public void doIt() throws UnknownHostException, FileNotFoundException, IOException, ClassNotFoundException {
        File[] projects = new File("E:\\jgithub").listFiles();

        Queue<Code> queue;
        File cacheDir = new File("cache" + File.separator + "j2es");
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        File cache = new File(cacheDir.getAbsolutePath() + File.separator + "queue.obj");
        if (cache.exists()) {
            FileInputStream fis = new FileInputStream(cache);
            ObjectInputStream ois = new ObjectInputStream(fis);
            queue = (ConcurrentLinkedDeque<Code>) ois.readObject();
        } else {
            ExecutorService es = Executors.newFixedThreadPool(100);
            CompletionService<Boolean> cs = new ExecutorCompletionService<>(es);
            int submitted = 0;
            queue = new ConcurrentLinkedDeque<>();
            for (File project : projects) {
                cs.submit(new JavaFinder(submitted, project, queue));
                submitted++;
            }
            int correct = 0;
            System.out.println("Submitted:" + submitted);

            for (int i = 0; i < submitted; i++) {
                try {
                    if (cs.take().get()) {
                        correct++;
                    }
                } catch (InterruptedException | ExecutionException e) {

                }
            }
            System.out.println("Correct:" + correct);
            FileOutputStream fos = new FileOutputStream(cache);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(queue);
            es.shutdown();
        }

        ObjectMapper mapper = new ObjectMapper();
        try (Client client = TransportClient.builder().build()
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300))) {
            BulkRequestBuilder bulkRequest = client.prepareBulk();
            int size = 1000;
            for (Code newCode : queue) {
                try {
                    bulkRequest.add(client.prepareIndex("sedocs", "code")
                            .setSource(mapper.writeValueAsBytes(newCode))
                    );
                    size--;
                } catch (Exception e) {
                    continue;
                }
                if (size == 0) {
                    size = 1000;
                    BulkResponse bulkResponse = bulkRequest.execute().actionGet();
                    if (bulkResponse.hasFailures()) {
                        System.out.println(bulkResponse.buildFailureMessage());
                    }
                    bulkRequest = client.prepareBulk();
                }
            }
            BulkResponse bulkResponse = bulkRequest.execute().actionGet();
            if (bulkResponse.hasFailures()) {
                System.out.println(bulkResponse.buildFailureMessage());
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Java2ElasticSearch app = new Java2ElasticSearch();
        app.doIt();
    }
}

class JavaFinder implements Callable<Boolean> {

    private final static String PREFIX = "https://github.com/";
    private final int num;
    private final File dir;
    private final Queue queue;

    public JavaFinder(int num, File dir, Queue queue) {
        this.num = num;
        this.dir = dir;
        this.queue = queue;
    }

    @Override
    public Boolean call() throws Exception {
        final String projectName = dir.getName().replace("--", "/") + "/tree/master/";
        final Path dirPath = Paths.get(dir.toURI());

        try {

            Files.walkFileTree(Paths.get(dir.toURI()), new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    if (attrs.isRegularFile() & file.toString().endsWith(".java")) {
                        Path relative = dirPath.relativize(file);
                        Code newCode = new Code(PREFIX + projectName + relative.toString().replace("\\", "/"), new String(Files.readAllBytes(file)));
                        queue.add(newCode);
                    }
                    return CONTINUE;
                }
            });
            System.out.println(num);
            return true;
        } catch (Exception e) {
            System.out.println(num);
            return false;
        }
    }

}
