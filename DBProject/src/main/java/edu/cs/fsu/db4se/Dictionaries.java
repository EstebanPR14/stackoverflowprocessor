package edu.cs.fsu.db4se;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 *
 * @author JavierRicardo
 */
public class Dictionaries {

    private static List<String> english;
    private static List<String> java;

    public static List<String> getEnglish() throws Exception {
        if (english == null) {
            english = Files.readAllLines(Paths.get(ClassLoader.getSystemResource("common-words-en.txt").toURI()), Charset.forName("UTF-8"));
        }
        return english;
    }
}
