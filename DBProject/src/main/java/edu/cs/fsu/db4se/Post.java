/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.cs.fsu.db4se;

import java.io.Serializable;

/**
 *
 * @author Javier
 */
public class Post implements Serializable {

    private String id;
    private String url;
    private String text;

    public Post() {

    }

    public Post(String id, String url, String text) {
        this.id = id;
        this.url = url;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getText() {
        return text;
    }

}
