/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stackoverflowprocessor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeMap;

/**
 *
 * @author Esteban Parra - Javier Escobar
 */
public class Post {

    private String originalLine;
    private final TreeMap<String, String> attributes;

    /**
     * Default constructor
     */
    public Post() {
        attributes = new TreeMap<>();
    }

    /**
     * Creates a new Post with the specified attributes
     *
     * @param postItems items of the post
     * @param line original line of the post in the dataset
     */
    public Post(TreeMap<String, String> postItems, String line) {
        this.attributes = postItems;
        this.originalLine = line;
    }

    /**
     *
     * @param key key of the attribute to retrieve
     * @return the value of the attribute corresponding to the specified key.
     * Null if there is no attribute with that key
     */
    public String get(String key) {
        return attributes.get(key);
    }

    /**
     *
     * @return the set of attributes of this post
     */
    public TreeMap<String, String> getAttributes() {
        return attributes;
    }

    /**
     *
     * @return the original line of this post
     */
    public String getOriginalLine() {
        return originalLine;
    }

    /**
     *
     * @return the set of tags of this post
     */
    public ArrayList<String> getTags() {
        ArrayList<String> tags = new ArrayList();
        if (attributes.get("Tags") != null) {
            String tagsLine = attributes.get("Tags");
            tags.addAll(Arrays.asList(tagsLine.split(";")));
        }
        return tags;
    }

    @Override
    public String toString() {
        String result = "";
        String newLine = System.getProperty("line.separator");
        TreeMap<String, String> items = attributes;
        for (String item : items.keySet()) {
            if (item.equals("Title") || item.equals("Body")) {
                String value = items.get(item) + newLine;
                result = result.concat(value);
            }
        }
        return result;
    }
}
