package stackoverflowprocessor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.util.Version;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;

/**
 *
 * @author Esteban Parra - Javier Escobar
 */
public class StackOverflowProcessor {

    /**
     * @param args
     */
    public static void main(String[] args) {
        String inputFilename = "F:\\Stackoverflow\\Posts.xml";
        processFile(inputFilename);
    }

    /**
     * @param filename
     */
    public static void processFile(String filename) {

        File file = new File(filename);
        Charset charset = Charset.forName("UTF-8");
        try (BufferedReader reader = Files.newBufferedReader(file.toPath(), charset)) {
            String currentLine;
            while ((currentLine = reader.readLine()) != null) {
                System.out.println(currentLine);
                Post p = transformLine(currentLine);
                if (p.get("Id") != null) {
                    processPost(p);
                }
            }
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
    }

    /**
     * @param post
     */
    public static void createFile(Post post) {

        String filename = "F:\\Stackoverflow\\Documents\\" + post.get("Id") + ".txt";
        File file = new File(filename);
        Charset charset = Charset.forName("UTF-8");

        try (BufferedWriter writer = Files.newBufferedWriter(file.toPath(), charset)) {
            String data;
            data = post.toString();
            writer.write(data, 0, data.length());

        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
    }

    /**
     * Method that retrieves the words inside a given string
     *
     * @param string to be splitted into words
     * @return A Set of words extracted from the input string
     */
    public TreeSet<String> splitIntoWords(String string) {
        TreeSet<String> splittedIdentifier = splitBy(string, " ");
        return splittedIdentifier;
    }

    /**
     *
     * @param string
     * @param regex
     * @return
     */
    public static TreeSet<String> splitBy(String string, String regex) {
        TreeSet<String> splittedIdentifier = new TreeSet<>();
        splittedIdentifier.addAll(Arrays.asList(string.split(regex)));
        splittedIdentifier.remove("");
        return splittedIdentifier;
    }

    /**
     * Method that splits an identifier.<br>
     * Example: <br>
     * identifier --> [identifier]<br>
     * myIdentifier --> [Identifier, my]<br>
     * Identifier --> [Identifier]<br>
     * my_Identifier --> [Identifier, my]<br>
     * my_identifier --> [identifier, my]<br>
     * my_IDENTIFIER --> [IDENTIFIER, my]<br>
     *
     * @param identifier identifier to be splitted
     * @return A set of strings. A set containing only the original identifier
     * if it cannot be separated
     */
    public TreeSet<String> splitIdentifier(String identifier) {
        identifier = identifier.replaceAll("\\n", "");

        ArrayList<String> splittedIdentifierList = new ArrayList<>();

        if (identifier.contains(" ")) {
            splittedIdentifierList.addAll(splitIntoWords(identifier));
        } else {
            splittedIdentifierList.addAll(Arrays.asList(identifier.split("_")));
            splittedIdentifierList.addAll(Arrays.asList(identifier.split("(?=\\p{Upper})")));
        }

        for (int i = 0; i < splittedIdentifierList.size(); i++) {
            String token = splittedIdentifierList.get(i);
            token = removeSpecialCharacters(token);
            splittedIdentifierList.set(i, token);
            if (token.length() <= 1) {
                splittedIdentifierList.remove(token);
                i--;
            }
        }

        if (splittedIdentifierList.size() >= 1) {
            splittedIdentifierList.remove(identifier);
        } else {
            splittedIdentifierList.add(identifier);
        }
        splittedIdentifierList.remove("");

        TreeSet<String> splittedIdentifier = new TreeSet<>(splittedIdentifierList);
        if (splittedIdentifier.size() > 1) {
            identifier = removeSpecialCharacters(identifier);
            splittedIdentifier.remove(identifier);
        }

        System.out.println(identifier + " --> " + splittedIdentifier);
        return splittedIdentifier;
    }

    /**
     * Method to remove special characters from a given string
     *
     * @param string string from which special character are to be removed
     * @return The given string without special characters
     */
    public static String removeSpecialCharacters(String string) {
        ArrayList<String> invalidStrings = new ArrayList<>();
        invalidStrings.add("&lt;p&gt;");
        invalidStrings.add("&lt;/p&gt;");
        invalidStrings.add("&lt;br&gt;");
        invalidStrings.add("&lt;/br&gt;");
        invalidStrings.add("&lt;");
        invalidStrings.add("p&gt;");
        invalidStrings.add("&#xA;");
        invalidStrings.add("&#xD;");
        invalidStrings.add("&lt;code&gt;");
        invalidStrings.add("&lt;/code&gt;");
        invalidStrings.add("&lt;pre&gt;");
        invalidStrings.add("&lt;/pre&gt;");
        invalidStrings.add("&lt;strong&gt;");
        invalidStrings.add("&lt;/strong&gt;");

        for (String s : invalidStrings) {
            string = string.replaceAll(s, "");
        }
        string = string.replace("&quot;", "\"");
        string = string.replace("&amp;", "&");

        string = Normalizer.normalize(string, Normalizer.Form.NFD).replaceAll("[^a-zA-Z0-9 ]", "");
        string = string.replaceAll("\"", "");
        return string;
    }

    /**
     * Method to replace special characters from a given string with blank
     * spaces
     *
     * @param string string from which special character are to be removed
     * @return The given string without special characters
     */
    public String replaceSpecialCharacters(String string) {
        string = Normalizer.normalize(string, Normalizer.Form.NFD).replaceAll("[^a-zA-Z ]", " ");
        return string;
    }

    /**
     *
     * @param line
     * @return
     */
    public static String formatTags(String line) {
        String tags = line.replaceAll("&gt;&lt;", ";");
        tags = tags.replaceAll("&lt;", "");
        tags = tags.replaceAll("\"", "");
        tags = tags.replaceAll("&gt;", "");
        return tags;
    }

    /**
     *
     * @param post
     * @param tag
     * @return
     */
    public static boolean containsTag(Post post, String tag) {
        ArrayList<String> tags = post.getTags();
        if (tags.size() > 0) {
            return tags.contains(tag);
        } else {
            return false;
        }
    }

    /**
     *
     * @param line
     * @return
     */
    public static Post transformLine(String line) {
        String originalLine = line;
        TreeMap<String, String> values = new TreeMap();
        Post post;
        if (line.startsWith("  <row ")) {
            line = line.replace("  <row ", "");
            line = line.replace("/>", "");

            TreeSet<String> postItems = splitBy(line, "\" ");
            postItems.stream().forEach((item) -> {
                if (item.startsWith("Body=")) {
                    String key = "Body";
                    String value = item.replace("Body=", "");
                    value = removeSpecialCharacters(value);
                    values.put(key, value);
                } else {
                    List<String> spllit = Arrays.asList(item.split("="));
                    if (spllit.size() == 2) {
                        String key = removeSpecialCharacters(spllit.get(0));
                        String value;
                        if (key.equals("Tags")) {
                            value = formatTags(spllit.get(1));
                        } else {
                            value = removeSpecialCharacters(spllit.get(1));
                        }
                        values.put(key, value);
                    }
                }
            });

            post = new Post(values, originalLine);
        } else {
            post = new Post();
        }
        return post;
    }

    /**
     * Method to evaluate and
     *
     * @param post
     */
    public static void processPost(Post post) {
        if (!post.getAttributes().isEmpty()) {
            //is a question
            if (containsTag(post, "java") && post.get("PostTypeId").equals("1")) {
                createFile(post);
            } else {
                //is an answer
                handleAnswer(post);
            }
        }
    }

    /**
     * Method to process and handle an answer post
     *
     * @param post an answer post
     */
    public static void handleAnswer(Post post) {
        String filename = "F:\\Stackoverflow\\Documents\\" + post.get("ParentId") + ".txt";
        File file = new File(filename);
        if (file.exists()) {
            appendAnswer(post);
        } else {
            saveForLater(post);
        }
    }

    /**
     * Method to append a question's content to the corresponding answer
     *
     * @param post an answer post
     */
    public static void appendAnswer(Post post) {
        String filename = "F:\\Stackoverflow\\Documents\\" + post.get("ParentId") + ".txt";
        try (FileWriter fw = new FileWriter(filename, true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {
            out.println("\n");
            out.println(post.toString());
        } catch (IOException ex) {
            Logger.getLogger(StackOverflowProcessor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Method to save unprocessed posts for later processing in an additional
     * file
     *
     * @param post post to be saved
     */
    public static void saveForLater(Post post) {
        String filename = "queued.txt";
        File file = new File(filename);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(StackOverflowProcessor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try (FileWriter fw = new FileWriter(filename, true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {
            out.println("");
            out.println(post.getOriginalLine());
        } catch (IOException ex) {
            Logger.getLogger(StackOverflowProcessor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param input
     * @return
     */
    private static String tokenizeStopStem(String input) {

        TokenStream tokenStream = new StandardTokenizer();
        tokenStream = new StopFilter(tokenStream, StopAnalyzer.ENGLISH_STOP_WORDS_SET);
        tokenStream = new PorterStemFilter(tokenStream);

        StringBuilder sb = new StringBuilder();
        OffsetAttribute offsetAttribute = tokenStream.addAttribute(OffsetAttribute.class);
        CharTermAttribute charTermAttr = tokenStream.getAttribute(CharTermAttribute.class);
        try {
            while (tokenStream.incrementToken()) {
                if (sb.length() > 0) {
                    sb.append(" ");
                }
                sb.append(charTermAttr.toString());
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return sb.toString();
    }
}
